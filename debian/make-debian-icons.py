#!/usr/bin/env python3

import subprocess
import xml.etree.ElementTree as ET

tree = ET.parse('icons.svg')
root = tree.getroot()

root.attrib['width'] = root.attrib['height']

for elt in list(root):
    if elt.tag == '{http://www.w3.org/2000/svg}g' and elt.attrib['id'] == 'curses icon':
        root.remove(elt)
    if elt.tag == '{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}namedview':
        elt.attrib['width'] = elt.attrib['height']

tree.write('icon.svg')

subprocess.check_call(["convert","-background","transparent","icon.svg","icon.png"])
