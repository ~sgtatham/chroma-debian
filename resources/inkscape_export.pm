our $inkscape_export_option = undef;

sub inkscape_export {
    my ($opts, $width, $height, $outfile, $infile) = @_;
    unless (defined $inkscape_export_option) {
        my $ver = `inkscape --version`;
        $inkscape_export_option = ($ver =~ /Inkscape 0\./) ? "-e" : "-o";
    }

    system "inkscape $opts -w $width -h $height $inkscape_export_option $outfile $infile";
}

1;
